
create database portfolio;
use portfolio;

create table lenguajes(
	id int primary key auto_increment,
    img varchar(100)
);

create table galeria(
	id int primary key auto_increment,
    img varchar(100)
);

create table sobre_mi(
	id int primary key auto_increment,
    nombre varchar (255),
    descripcion text,
    img varchar(100)
);

create table contacto(
	id int primary key auto_increment,
    nombre varchar (100) not null,
    email varchar (100),
    mensaje text
);

insert into sobre_mi values (1,"Adrián Vega","Soy un programador con amplios conocimientos en HTML y CSS. Creativo y profesional, con gran visión espacial y trabajador en equipo.", './imagenes/MicrosoftTeams-image.png');

insert into galeria values (3,"./imagenes/Ciervos3.png");
insert into galeria values (2, "./imagenes/Ejercicio Grid.png");
insert into galeria values (1, "./imagenes/Ejercicio Clase 2023-11-09 192249.png");

insert into lenguajes values (1,"./imagenes/html5.png");
insert into lenguajes values (2,"./imagenes/CSS3_logo.png");
insert into lenguajes values (3,"./imagenes/Java_programming.png");

