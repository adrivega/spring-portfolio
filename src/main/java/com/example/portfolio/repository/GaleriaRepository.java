package com.example.portfolio.repository;

import com.example.portfolio.model.Galeria;
import org.springframework.data.jpa.repository.JpaRepository;
public interface GaleriaRepository extends JpaRepository<Galeria, Long> {
}
