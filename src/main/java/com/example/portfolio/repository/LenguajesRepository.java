package com.example.portfolio.repository;

import com.example.portfolio.model.Lenguajes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LenguajesRepository extends JpaRepository<Lenguajes, Long> {
}
