package com.example.portfolio.repository;

import com.example.portfolio.model.Sobre_mi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Sobre_miRepository extends JpaRepository<Sobre_mi, Long> {
}
