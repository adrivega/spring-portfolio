package com.example.portfolio.controller;

import com.example.portfolio.model.Contacto;
import com.example.portfolio.model.Sobre_mi;
import com.example.portfolio.model.Galeria;
import com.example.portfolio.model.Lenguajes;
import com.example.portfolio.model.Galeria;
import com.example.portfolio.model.Lenguajes;
import com.example.portfolio.model.Sobre_mi;
import com.example.portfolio.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BasicController {
    @Autowired
    private ContactoService contactoService;
    @Autowired
    private Sobre_miService sobre_miService;
    @Autowired
    private GaleriaService galeriaService;
    @Autowired
    private LenguajesService lenguajesService;


    @GetMapping("/")
    String vertodo(Model model) {
        List<Sobre_mi> Sobre_miList = sobre_miService.getAllSobre_mi();
        model.addAttribute("Sobremi",Sobre_miList);

        List<Galeria> GaleriaList = galeriaService.getAllGalerias();
        model.addAttribute("Galeria",GaleriaList);

        List<Lenguajes> LenguajesList = lenguajesService.getAllLenguajes();
        model.addAttribute("Lenguajes",LenguajesList);

        return "Portfolio";
    }

    @PostMapping("/")
    String todo(@RequestParam String nombre,@RequestParam String email,@RequestParam String mensaje, Model model) {
        Contacto contact=new Contacto();
        contact.setNombre(nombre);
        contact.setEmail(email);
        contact.setMensaje(mensaje);

        contactoService.addContacto(contact);

        List<Sobre_mi> Sobre_miList = sobre_miService.getAllSobre_mi();
        model.addAttribute("Sobre mi",Sobre_miList);

        List<Galeria> GaleriaList = galeriaService.getAllGalerias();
        model.addAttribute("Galeria",GaleriaList);

        List<Lenguajes> LenguajesList = lenguajesService.getAllLenguajes();
        model.addAttribute("Lenguajes",LenguajesList);

        return "Portfolio";
    }
}