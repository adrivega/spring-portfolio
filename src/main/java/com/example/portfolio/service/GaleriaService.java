package com.example.portfolio.service;

import com.example.portfolio.model.Galeria;
import com.example.portfolio.repository.GaleriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class GaleriaService {
    @Autowired
    private GaleriaRepository galeriaRepository;
    public List<Galeria> getAllGalerias() {
        return galeriaRepository.findAll();
    }

    public Galeria getGaleriaById(Long id) {
        return galeriaRepository.findById(id).orElse(null);
    }
}
