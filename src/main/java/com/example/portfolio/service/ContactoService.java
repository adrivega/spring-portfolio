package com.example.portfolio.service;

import com.example.portfolio.model.Contacto;
import com.example.portfolio.repository.ContactoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;
    public List<Contacto> getAllContactos() {
        return contactoRepository.findAll();
    }


    public Contacto getContactoById(Long id) {
        return contactoRepository.findById(id).orElse(null);
    }

    public Contacto addContacto(Contacto contacto) {
        return contactoRepository.save(contacto);
    }
}
