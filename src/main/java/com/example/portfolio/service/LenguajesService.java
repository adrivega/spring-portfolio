package com.example.portfolio.service;

import com.example.portfolio.model.Galeria;
import com.example.portfolio.model.Lenguajes;
import com.example.portfolio.repository.GaleriaRepository;
import com.example.portfolio.repository.LenguajesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LenguajesService {
    @Autowired
    private LenguajesRepository lenguajesRepository;
    public List<Lenguajes> getAllLenguajes() {
        return lenguajesRepository.findAll();
    }

    public Lenguajes getLenguajesById(Long id) {
        return lenguajesRepository.findById(id).orElse(null);
    }
}
