package com.example.portfolio.service;

import com.example.portfolio.model.Lenguajes;
import com.example.portfolio.model.Sobre_mi;
import com.example.portfolio.repository.LenguajesRepository;
import com.example.portfolio.repository.Sobre_miRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class Sobre_miService {
    @Autowired
    private Sobre_miRepository sobreMiRepository;
    public List<Sobre_mi> getAllSobre_mi() {
        return sobreMiRepository.findAll();
    }

    public Sobre_mi getSobre_miById(Long id) {
        return sobreMiRepository.findById(id).orElse(null);
    }
}
