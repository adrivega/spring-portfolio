package com.example.portfolio.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="galeria")
public class Galeria {

    @Id
    private Long id;
    private String img;
    private String lenguaje_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLenguaje_id() {
        return lenguaje_id;
    }

    public void setLenguaje_id(String lenguaje_id) {
        this.lenguaje_id = lenguaje_id;
    }
}
